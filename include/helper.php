<?php

// Recreation of mysql_result in mysqli
function mysqli_result($res,$row=0,$col=0)
{ 
    $numrows = mysqli_num_rows($res); 
    if ($numrows && $row <= ($numrows-1) && $row >=0) {
        mysqli_data_seek($res, $row);
        $resrow = (is_numeric($col)) ? mysqli_fetch_row($res) : mysqli_fetch_assoc($res);
        if (isset($resrow[$col])) {
            return $resrow[$col];
        }
    }
    return false;
}

function fulltext_search($req, $columns, $full_phrase)
{
    $where = array();
    $match = '';

    if (!preg_match('|^[0-9A-Fa-f]{32}$|', $req)) {
        preg_match_all('~(978[0-9]{9}[0-9X]{1}|[0-9]{9}[0-9X]{1})~', str_replace('-', '', $req), $isbnfindname, PREG_PATTERN_ORDER);
    }
    if (count($isbnfindname[0]) > 0) {
        //$isbnfindname1 = array_fill_keys($isbnfindname[0], '');
        $req = preg_replace('~[0-9Xx\-]{10,17}~', '', $req); //убираем isbn из поискового запроса, все прочие слова ищем отдельно и isbn отдельно
    }

    preg_match_all('|topicid[0-9]{1,3}|', $req, $topicfindname, PREG_PATTERN_ORDER);
    if (count($topicfindname[0]) > 0) {
        $req = preg_replace('~topicid[0-9]{1,3}~', '', $req);
    }

    //если ISBN найден в поисковой строке, то колонку Identifier из дальнейшего поиска исключаем, тк ISBN ищутся позже отдельно
    if (preg_match('|[A-Za-zА-Яа-я0-9]|', $req) && !empty($columns) && $columns != 'identifier' && $columns != 'topic') {
        $req = preg_replace('/[[:punct:]]+/u', ' ', $req); //для подстановки в sql запрос
        $req = preg_replace('/[\s]+/u', ' ', trim($req));

        $search_words = array_diff(explode(' ', mb_strtolower($req, 'UTF-8')),  array("and", "the","of","from","to", "in", "it", "on"));
        //$search_words = explode(' ', strtolower($req));

        if ($columns == 'language') {
            $where[] = "MATCH(`" . $columns . "`) AGAINST('+" . mysqli_real_escape_string($req) . "' IN BOOLEAN MODE)";
        }
        else if ($columns == 'extension') {
            $where[] = "MATCH(`" . $columns . "`) AGAINST('+" . mysqli_real_escape_string($req) . "' IN BOOLEAN MODE)";
        }
        else if ($columns == 'md5') {
            if (preg_match('|^[0-9A-Fa-f]{32}$|', $req)) {
                $where[] = "`MD5`='" . $req . "'";
            } else {
                die($htmlhead . "<font color='#A00000'><h1>Wrong MD5</h1></font>" . $htmlfoot);
            }
        }
        else
        {
            foreach ($search_words as $search_word)
            {
                if (preg_match('~^[0-9]{1,3}$~', $search_word)) //для случая, если ищется номер, который может быть записан с 0 в начале
                {
                    $sql_word[] = "+(" . ltrim($search_word, "0") . " 0" . ltrim($search_word, "0") . " 00" . ltrim($search_word, "0") . " 000" . ltrim($search_word, "0") . ")";
                }
                else
                {
                    if ($full_phrase == '1' || mb_strlen($search_word, 'UTF-8') == 1) {
                        $sql_word[] = '+' . $search_word;
                    } else {
                        $sql_word[] = '+' . $search_word . '*';
                    }
                }
            }
            if ($columns == 'def') {
                $match_columns = '`title`,`author`,`series`,`publisher`,`year`,`periodical`,`volumeinfo`';
            } else {
                $match_columns = '`' . $columns . '`';
            }
            $match = "MATCH(" . $match_columns . ") AGAINST('" . join(' ', $sql_word) . "' IN BOOLEAN MODE)";
        }
    }

    if (!empty($isbnfindname[0])) { // дописываем условие поиска по ISBN, если указана колонка
        $where[] = "MATCH(`IdentifierWODash`) AGAINST ('+" . implode(' +', $isbnfindname[0]) . "' IN BOOLEAN MODE)";
    }
    if (!empty($topicfindname[0])) { // дописываем условие поиска по ISBN, не зависимо от того задана ли такая колонка
        $where[] = "`Topic` IN ('" . str_replace('topicid', '', implode("','", $topicfindname[0])) . "')";
    }
    //    if ((empty($isbnfindname[0]) && $columns == 'identifier') || (empty($topicfindname) && $columns == 'topic'))
    //        $where = '';

    return array('where' => $where, 'match' => $match);
}