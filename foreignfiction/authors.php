﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="robots" content="noindex,nofollow">
    <link rel="stylesheet" href="../menu.css" type="text/css" media="screen"/>
    <title>Library Genesis: Foreign Fiction</title>
    <!--[if IE 6]>
    <style>
        body { behavior: url('../csshover3.htc'); }
        #menu li .drop { background:url('img/drop.gif') no-repeat right 8px; }
    </style>
    <![endif]-->
</head>
<?php
$lang = isset($_COOKIE['lang']) && preg_match('/^[a-z]{2}$/', $_COOKIE['lang']) ? $_COOKIE['lang'] : 'en';
$lang_file = 'lang_' . $lang . '.php';
if (!file_exists($lang_file)) {
    $lang_file = 'lang_en.php';
}

require_once '../config.php';
require_once 'menu_' . $lang . '.php';

if (!isset($_GET['letter']) || !preg_match('|^[a-zA-Z0-9]$|', $_GET['letter'])) {
    echo '<font color="#A00000"><h1>Invalid letter value</h1></font>';
    exit();
}

$result = mysqli_query($con, "SELECT TRIM(CONCAT(`AuthorFamily1`,' ',`AuthorName1`,' ',`AuthorSurname1`)) AS `author`,COUNT(*) AS `count` FROM `main` WHERE `AuthorFamily1` LIKE '" . mysqli_real_escape_string(strtoupper($_GET['letter'])) . "%' GROUP BY `AuthorFamily1`,`AuthorName1`,`AuthorSurname1` ORDER BY `author`");
echo '<br><table width=100% align=center rules=rows><thead>
<tr><td colspan="2" align="center"><font color="#A00000"><h1><a href="/">Library Genesis:</a> <a href="/foreignfiction/">Foreign fiction</a> <sup><font size="4">1M</font></sup></h1></font></td></tr>
<tr><td width="70%"><b>Author</b></td><td><b>How many books</b></td></tr></thead><tbody>';
while ($row = mysqli_fetch_assoc($result)) {
    echo '<tr><td><a href="index.php?s=' . urlencode($row['author']) . '&f_lang=0&f_columns=2&f_group=1">' . htmlspecialchars($row['author'], ENT_QUOTES) . '</a></td><td>' . $row['count'] . '</td></tr>';
}
echo '</tbody></table>';
mysqli_free_result($result);
mysqli_close($con);
?>
</body>
</html>