<?php

//This ensures $url is set, so if not in the root of a webserver, it can still operate
require_once 'config.php';

$htmlhead = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
	<META HTTP-EQUIV='CACHE-CONTROL' CONTENT='NO-CACHE'>
	<meta name='robots' content='noindex,nofollow'>
	<meta name='description' content='Library Genesis is a scientific community targeting collection of books on natural science disciplines and engineering.'>
	<meta name='rating' content='general'>

	<link rel='stylesheet' href='" . $url . "/menu.css' type='text/css' media='screen' />
	<title>Library Genesis</title>
	<!--[if IE 6]>
	<style>
		body {behavior: url('" . $url . "/csshover3.htc');}
		#menu li .drop {background:url('img/drop.gif') no-repeat right 8px; 
	</style>
	<![endif]-->
</head>";


$htmlfoot = <<<HTML
</body></html>
HTML;
